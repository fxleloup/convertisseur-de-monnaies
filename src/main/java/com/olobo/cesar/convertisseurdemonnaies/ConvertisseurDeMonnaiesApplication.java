package com.olobo.cesar.convertisseurdemonnaies;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConvertisseurDeMonnaiesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConvertisseurDeMonnaiesApplication.class, args);
	}

}
