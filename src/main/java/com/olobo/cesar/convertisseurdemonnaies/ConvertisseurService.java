package com.olobo.cesar.convertisseurdemonnaies;

import com.ritaja.xchangerate.api.CurrencyConverter;
import com.ritaja.xchangerate.api.CurrencyConverterBuilder;
import com.ritaja.xchangerate.util.Currency;
import com.ritaja.xchangerate.util.Strategy;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class ConvertisseurService {

    public Double calculer_change(
            String monnaieSource,
            String monnaieCible) {

        return calculer_change_distant(monnaieSource, monnaieCible);
    }


    public Double calculer_change_distant(
            String monnaieSource,
            String monnaieCible) {

        CurrencyConverter converter = new CurrencyConverterBuilder()
                .strategy(Strategy.CURRENCY_LAYER_FILESTORE)
                .accessKey("25d24a13dbc174a00e295fb5f0cad0ef")
                .buildConverter();

        converter.setRefreshRateSeconds(86400);

        try {
            return converter.convertCurrency(
                            new BigDecimal("1"),
                            Currency.valueOf(monnaieSource),
                            Currency.valueOf(monnaieCible))
                    .doubleValue();

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    public Double calculer_change_local(
            String monnaieSource,
            String monnaieCible) {

        if ("EUR".equals(monnaieSource) && "CHF".equals(monnaieCible)) {
            return EUR_CHF;
        }
        if ("USD".equals(monnaieSource) && "EUR".equals(monnaieCible)) {
            return USD_EUR;
        }
        if ("USD".equals(monnaieSource) && "BRL".equals(monnaieCible)) {
            return USD_BRL;
        }
        if ("CHF".equals(monnaieSource) && "BRL".equals(monnaieCible)) {
            return CHF_BRL;
        }
        return -1.0;
    }

    Double EUR_CHF = 0.9305;
    Double USD_EUR = 0.9133;
    Double USD_BRL = 4.8993;
    Double CHF_BRL = 5.7660;
}
